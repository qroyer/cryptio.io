DC=docker-compose

.DEFAULT_GOAL := help

.PHONY: help

help:  ##Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

up: ##Start Docker
	$(DC) up -d --build --force-recreate

ps: ##Start Docker
	$(DC) ps

do: ##Stop Docher
	$(DC) down

restart: ##Restart docker
	$(DC) down && $(DC) up -d

ex: ##Connect to PHP
	$(DC) exec --user=www-data app /bin/bash

db: ##Connect to PHP
	$(DC) exec --user=www-data db /bin/bash

dba: ##Connect to PHP
	$(DC) exec --user=root db /bin/bash

initdb:
	$(DC) exec app bash -c "DATABASE_URL=\"postgresql://database:database@db:5432/crypto?serverVersion=13&charset=utf8\" crypto_back/bin/console doctrine:database:drop --if-exists --force"
	$(DC) exec app bash -c "DATABASE_URL=\"postgresql://database:database@db:5432/crypto?serverVersion=13&charset=utf8\" crypto_back/bin/console doctrine:database:create"
	$(DC) exec app bash -c "DATABASE_URL=\"postgresql://database:database@db:5432/crypto?serverVersion=13&charset=utf8\" crypto_back/bin/console make:migration"
	$(DC) exec app bash -c "DATABASE_URL=\"postgresql://database:database@db:5432/crypto?serverVersion=13&charset=utf8\" crypto_back/bin/console doctrine:migration:migrate -n"

exa: ##Connect to PHP Admin
	$(DC) exec --user=root app /bin/bash

logs: ##Connect to PHP
	$(DC) logs -f

ai: ##Install Assets with webpack-encore
	$(DC) run app yarn install

ab: ##Build Assets with webpack-encore
	$(DC) run app ./node_modules/.bin/encore production

cc: ##Clean cache
	$(DC) exec app php bin/console c:c

reset_db: ##
	$(DC) exec app bash -c "crypto_back/bin/console doctrine:database:drop --if-exists --force"
	$(DC) exec app bash -c "crypto_back/bin/console doctrine:database:create"

