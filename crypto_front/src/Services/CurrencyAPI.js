import axios from 'axios';

export const API_ENDPOINT = 'http://localhost/api';
export const API_ENDPOINT_COINS = 'http://localhost';

export async function getAllCryptoCurrencies() {
    return await axios.get(`${API_ENDPOINT_COINS}/coins/markets`);
}

export async function testAPI() {
    return await axios.get(`${API_ENDPOINT}/ping`);
}

export async function getUser(pseudo) {
    return await axios.get(`${API_ENDPOINT}/user/pseudo/` + pseudo);
}

export async function postUser(pseudo, password) {
    return await axios.post(`${API_ENDPOINT}/users`, {
        "pseudo": pseudo,
        "password": password
    });
}

export async function getCryptoStored() {
    return await axios.get(`${API_ENDPOINT}/crypto_currencies`);
}

export async function getCryptoBySymbol(symbol) {
    return await axios.get(`${API_ENDPOINT}/crypto_currencies/symbol/` + symbol);
}

export async function postCryptoBySymbol(name, symbol) {
    return await axios.post(`${API_ENDPOINT}/crypto_currencies`,{
        name: name,
        symbol: symbol
    });
}

export async function postWallet(amount, user, crypto) {
    return await axios.post(`${API_ENDPOINT}/wallets`,{
        "amout": parseFloat(amount),
        "userId": '/api/users/'+ user,
        "cryptoId": '/api/crypto_currencies/'+ crypto
    });
}

export async function getCoinsInfoByName(name) {
    return await axios.get(`${API_ENDPOINT_COINS}/coins/markets/` + name);
}
