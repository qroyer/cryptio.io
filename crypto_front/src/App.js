import logo from './logo.svg';
import './css/App.css';
import {Route, Routes} from "react-router-dom";
import Navigation from './Page/Navigation';
import Dashboard from "./Dashboard/Dashboard";
import Login from "./Authentification/Login"
import Currencies from "./Currencies/Currencies"
import Account from "./Authentification/Account";
import Register from "./Authentification/Register";

function App() {
    return (
        <div className="main-content">
            <Navigation/>
            <Routes>
                <Route path="/login" element={<Login/>}/>
                <Route path="/dashboard" element={<Dashboard/>}/>
                <Route path="/invests" element={<Currencies/>}/>
                <Route path="/account" element={<Account/>}/>
                <Route path="/register" element={<Register/>}/>
            </Routes>
        </div>
    );
}

export default App;
