import Navigation from './Page/Navigation'
import logo from './css/logo.svg' // relative path to image

function Menu() {
    return (
        <div className="side-menu flex-row flex mx-10">
            <img id="logo" src={logo} alt={"logo"}/>
        </div>
    );
}

export default Menu;
