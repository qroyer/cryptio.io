import SearchBar from './../Component/SearchBar';
import CurrencyList from './../Component/CurrencyList';
import {useState} from 'react';
import MustBeLogged from "../Authentification/MustBeLogged";


function Currencies() {
    const [currencyFilter, setCurrencyFilter] = useState(null);

    return (
        <div className="search-block max-w-container mx-auto">
            <MustBeLogged/>
            <div className="submenu">
                <SearchBar placeholder="Enter a currency name" callback={setCurrencyFilter}/>
            </div>
            <CurrencyList filter={currencyFilter}/>
        </div>
    );
}

export default Currencies;
