import React from 'react';
import RecentChangesItem from "../Component/RecentChangesItem";

const RecentChanges = (props) => {
    const {data} = props;
    return (
        <div id={"rc"} className={"w-1/2 overflow-scroll p-5 h-600p"}>
            {
                data
                    .map((element) => {
                        let elem = element[0];
                        return (
                            <RecentChangesItem key={elem.id} name={elem.name} price={elem.current_price}
                                               symbol={elem.symbol} image={elem.image}
                                               change={elem.ath_change_percentage}/>
                        )
                    })
            }
        </div>
    );
}

export default RecentChanges;