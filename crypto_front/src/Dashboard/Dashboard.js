import Wallet from './Wallet';
import {useSelector} from "react-redux";
import MustBeLogged from "../Authentification/MustBeLogged";


const Dashboard = () => {

    const userPseudo = useSelector((state) => state.login.pseudo)

    return (
        <div className="max-w-container m-auto">
            <MustBeLogged />
            <h1 className="text-bold text-orange text-3xl">Dashboard</h1>
            <Wallet />

        </div>
    )
}

export default Dashboard;