import {useState} from "react";
import React, {useEffect} from 'react';
import { PieChart } from "react-minimal-pie-chart";
import Modal from "react-modal";

const Composition = (props) => {
    const {data, amount} = props;
    let values = [];
    for (const element of data) {
        let exist = false;
        let randomColor = "#000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});

        const member = {
            color: randomColor,
            title: element[1].id,
            value: element[2].proportion
        }

        for (const coins of values) {
            if (coins.title == element[1].id) {
                coins.proportion += element[2].proportion;
                exist = true;
            }
        }

        if (!exist) {
            values.push(member);
        }
    }

    return (
        <div className={"p-24 w-1/2"}>
            <PieChart
                animation
                animationDuration={500}
                animationEasing="ease-out"
                center={[50, 50]}
                data={values}
                labelPosition={50}
                lengthAngle={360}
                lineWidth={27}
                paddingAngle={0}
                radius={50}
                rounded
                startAngle={0}
                viewBoxSize={[100, 100]}
            />
        </div>
    );
}

export default Composition;