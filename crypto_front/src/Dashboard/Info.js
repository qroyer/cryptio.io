import React from 'react';
import {useSelector} from "react-redux";

const Composition = (props) => {

    const settingsCurrency = useSelector((state) => state.settingsCurrency.value)

    const {data} = props;

    let globalAmount = 0;
    for (const element of data) {
        globalAmount += element[2].proportion;
    }

    return (
        <div className="text-white text-2xl m-8">
            <ul className={"flex flex-row justify-around"}>
                <li className="bg-orange rounded-lg p-5">
                    <p>Valeur totale : {Math.round(globalAmount * 100) / 100} {settingsCurrency === 'eur' ? '€' : '$'}</p>
                </li>
            </ul>
        </div>
    );
}

export default Composition;