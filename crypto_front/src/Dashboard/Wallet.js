import {getCoinsInfoByName, getUser} from "../Services/CurrencyAPI";
import {useSelector} from "react-redux";
import React, {useEffect, useState} from "react";
import RecentChanges from "./RecentChanges";
import Composition from "./Composition";
import Info from "./Info";

function Wallet() {

    const userPseudo = useSelector((state) => state.login.pseudo)

    const [user, setUser] = useState([]);
    const [amount, setAmount] = useState(0);
    const [data, setData] = useState([]);
    const [recentChangesData, setRecentChangesData] = useState([]);

    let tmpArray = [];

    useEffect(() => {
        getUser(userPseudo).then((response) => {
            setUser(
                response.data
            );
            getGlobalAmount(response.data)

        });
    }, [amount]);


    async function countAmountInvested(user) {
        if (user.wallets !== undefined) {

            let arrayTest = [];
            for (const wallet of user.wallets) {
                await getCoinsInfoByName(wallet.cryptoId.name).then((response) => {
                    if (!isNaN(amount) && !isNaN(wallet.amout) && !isNaN(response.data[0].current_price)) {

                        if (!recentChangesData.includes(response.data)) {
                            tmpArray = recentChangesData
                            tmpArray.push(response.data);
                            setRecentChangesData(tmpArray);
                        }

                        const value = {
                            current_price: response.data[0].current_price,
                            id: wallet.cryptoId.name
                        }

                        const proportion =
                            {
                            proportion: response.data[0].current_price * wallet.amout
                        }

                        const dataArray = [
                            wallet,
                            value,
                            proportion
                        ]
                        arrayTest.push(dataArray)
                    }
                });
            }
            await setData(arrayTest)
        }
    }

    async function getGlobalAmount(user) {
        countAmountInvested(user).then();
        let globalAmount = 0;

        for (const element of data) {
            globalAmount += element[2].proportion;
        }
        setAmount(globalAmount);
    }

    return (
        <div className="wallet">
            <Info data={data} />
            <div className={"flex flex-row"}>
                <RecentChanges data={recentChangesData}/>
                <Composition data={data} />
            </div>
        </div>
    );
}

export default Wallet;
