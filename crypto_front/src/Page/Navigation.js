import {Navigate, NavLink} from 'react-router-dom';
import {useSelector, useDispatch} from 'react-redux'
import Menu from "../Menu";
import {log_in, log_off} from "../Authentification/loginSlice";
import React from "react";

const Navigation = () => {
    const login = useSelector((state) => state.login.value)
    const userPseudo = useSelector((state) => state.login.pseudo)
    const dispatch = useDispatch()

    const linksNotLoggedIn = [
        {to: "/login", key: "", label: "Login"},
        {to: "/register", key: "", label: "Register"},
        {to: "/invests", key: "invests", label: "Invests"},
        {to: "/dashboard", key: "dashboard", label: "Dashboard"}
    ]

    const linksLoggedIn = [
        {to: "/invests", key: "invests", label: "Invests"},
        {to: "/dashboard", key: "dashboard", label: "Dashboard"},
        {to: "/account", key: "", label: "My account"}
    ]

    let links = login ? linksLoggedIn : linksNotLoggedIn;

    function logging_off(event) {
        event.preventDefault();
        dispatch(log_off());
        return (<Navigate to='/login'/>)
    }

    return (
        <div className="text-orange">
            <div className="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
                <nav id="menu" className="md:ml-auto md:mr-auto flex flex-wrap items-center justify-center">
                    <Menu />
                    {
                        links.map(({to, key, label}) => {
                            return (
                                <a className="mr-5 text-orange hover:text-gray-900 text-xl uppercase">
                                    <NavLink to={to} key={key} className={({isActive}) => {
                                        return isActive ? 'active' : null
                                    }}>
                                        {label}
                                    </NavLink>
                                </a>
                            )
                        })
                    }

                    <button
                        className="inline-flex items-center bg-gray-100 border-0 py-1 px-3 focus:outline-none hover:bg-gray-200 rounded text-base mt-4 md:mt-0">
                        <span className="uppercase text-xl" onClick={logging_off}>Se déconnecter</span>
                    </button>
                </nav>
            </div>
        </div>

    )
}

export default Navigation;