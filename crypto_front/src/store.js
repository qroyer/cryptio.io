import { configureStore } from '@reduxjs/toolkit'
import loginReducer from './Authentification/loginSlice'
import settingsCurrencyReducer, {settingsCurrencySlice} from './Authentification/settingsCurrencySlice'

export default configureStore({
    reducer: {
        login: loginReducer,
        settingsCurrency: settingsCurrencyReducer,
    },
})