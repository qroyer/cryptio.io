import React, {useEffect} from 'react';
import {useState} from 'react';
import {getAllCryptoCurrencies} from "../Services/CurrencyAPI"
import CurrencyListItem from './../Component/CurrencyListItem'

function CurrencyList({filter}) {
    const [currency, setCurrency] = useState([]);

    useEffect(() => {
        getAllCryptoCurrencies().then((response) => {
            console.log(response.data)
            setCurrency(
                response.data.map((currency) => {

                    return {
                        id: currency.id,
                        name: currency.id,
                        symbol: currency.symbol,
                        rank: currency.market_cap_rank,
                        change: currency.market_cap_change_percentage_24h,
                        price: currency.sparkline_in_7d.price[currency.sparkline_in_7d.price.length - 1],
                        image: currency.image,
                        sparkline: currency.sparkline_in_7d
                    }
                })
            );
        });
    }, []);

    return (
        <table className="invests w-full">
            <thead>
            <tr>
                <th className={"text-orange"}>Symbol</th>
                <th className={"text-orange"}>Prix</th>
                <th className={"w-96 text-orange"}>Cours</th>
                <th className={"w-10 text-orange"}>Valorisation (- 24h)</th>
                <th className={"text-orange"}>Action</th>
            </tr>
            </thead>
            {
                currency
                    .filter(({name}) => filter ? name.includes(filter) : true)
                    .map(({name, symbol, id, image, rank, price, sparkline, change}) => {
                        return <CurrencyListItem key={id} name={name} price={Math.round(price * 100) / 100} symbol={symbol} image={image}
                                                 rank={rank} sparkline={sparkline} change={change}/>
                    })
            }
        </table>
    );
}

export default CurrencyList;
