import {useState} from 'react';

function SearchBar(props) {
    const [query, setQuery] = useState([]);
    return (
        <div className="search-block">
            <div className="m-auto flex justify-center">
                <input className="text-orange rounded-lg border-orange border-2" placeholder="Search crypto" onChange={
                    (event) => setQuery(event.target.value)
                }/>
                <button className="btn bg-orange mx-5 text-white px-3 rounded-lg py-1" onClick={() => props.callback(query)}>Chercher</button>
            </div>
        </div>
    );
}
export default SearchBar;
