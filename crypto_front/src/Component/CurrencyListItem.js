import {useState} from "react";
import React, {useEffect} from 'react';
import {Sparklines, SparklinesLine} from "react-sparklines";
import Modal from "react-modal";
import {getAllCryptoCurrencies, getCryptoStored, postCryptoBySymbol, postWallet, getUser} from "../Services/CurrencyAPI";
import {useSelector} from "react-redux";

Modal.setAppElement("#root");

const CurrencyListItem = (props) => {
    const {name, price, symbol, image, rank, sparkline, change} = props;
    const settingsCurrency = useSelector((state) => state.settingsCurrency.value)
    const userPseudo = useSelector((state) => state.login.pseudo)
    const [isOpen, setIsOpen] = useState(false);
    const [currencies, setCurrencies] = useState();
    const [errorMessages, setErrorMessages] = useState({});

    useEffect(() => {
        getCryptoStored().then((response) => {
            setCurrencies(response.data['hydra:member'])
        });
    }, []);

    function toggleModal() {
        setIsOpen(!isOpen);
    }

    function buyCrypto(event) {
        event.preventDefault();
        let amount = document.getElementById("amount");

        if(!(amount.value > 0 && amount.value !== '')) {
            setErrorMessages({name: "amount", message: errors.amount});
        } else {
            if (!isCryptoStored()) {
                postCrypto();
                getCryptoStored().then((response) => {
                    setCurrencies(response.data['hydra:member'])
                });
            }
            postWalletToBack()
        }
    }

    function isCryptoStored() {
        getCryptoStored().then((response) => {
            setCurrencies(response.data['hydra:member'])
        });

        Object.keys(currencies).forEach(key => {
            if (currencies[key].symbol === symbol) {
                return true;
            }
        });
        return false
    }

    function postCrypto() {
        postCryptoBySymbol(name, symbol).then((response) => {

        });
    }

    async function postWalletToBack() {
        let user = await getUser(userPseudo); //TODO
        let amount = document.getElementById("amount");

        let currency;

        Object.keys(currencies).forEach(key => {
            if (currencies[key].symbol === symbol) {
                currency = currencies[key];
            }
        });

        console.log("1 : ", amount.value, " 2: ", user.data, " 3: ", currency)

        postWallet(amount.value, user.data.id, currency.id).then((response) => {

        });
    }

    const errors = {
        amount: "invalid amount"
    };

    const renderErrorMessage = (name) =>
        name === errorMessages.name && (
            <div className="error">{errorMessages.message}</div>
        );

    return (
        <tr>
            <td className={"text-center"}><img className="mx-auto currency-image" src={image} alt={symbol}/> {symbol}</td>
            <td className={"text-center"}>{price} {settingsCurrency === 'eur' ? '€' : '$'}</td>
            <td >
                <Sparklines data={sparkline.price}>
                    <SparklinesLine color="#253e56"/>
                </Sparklines>
            </td>
            <td className={"text-center"}>{change} %</td>
            <td type="button">
                <button onClick={toggleModal} className={"m-[100%] p-[50%] rounded-lg text-center text-white bg-orange"}>
                    Buy
                </button>
                <Modal id={"modal"}
                    isOpen={isOpen}
                    onRequestClose={toggleModal}
                    contentLabel="Buy"
                >
                    <div className="p-5 relative">
                        <div className="flex flex-row">
                            <button className="rounded-lg bg-orange text-white text-bold p-1 absolute right-2 top-2" onClick={toggleModal}>Fermer</button>
                        </div>
                        <div className="ml-10 flex flex-col">
                            <div className={'text-bold text-lg'}>Acheter du {name}</div>
                            <div className={"flex flex-row"}>
                                <input className="text-orange rounded-lg border-orange border-2 p-1" type="number" required id="amount"
                                       minLength="4" maxLength="8" size="10"/>
                                {renderErrorMessage("amount")}
                                <button className="rounded-lg bg-orange text-white text-bold p-1" onClick={buyCrypto}>Acheter</button>
                            </div>
                        </div>
                    </div>
                </Modal>
            </td>

        </tr>
    );
}

export default CurrencyListItem;