import {useState} from "react";
import React, {useEffect} from 'react';
import {Sparklines, SparklinesLine} from "react-sparklines";
import Modal from "react-modal";
import {getCryptoStored, postCryptoBySymbol, postWallet, getUser} from "../Services/CurrencyAPI";
import {useSelector} from "react-redux";

Modal.setAppElement("#root");

const RecentChangesItem = (props) => {
    const {name, price, symbol, image, change} = props;

    const userPseudo = useSelector((state) => state.login.pseudo)

    const [isOpen, setIsOpen] = useState(false);
    const [currencies, setCurrencies] = useState();
    const [errorMessages, setErrorMessages] = useState({});

    return (
        <tr className={"flex flex-row justify-between border-b border-orange my-5"}>
            <td><img className="currency-image" src={image} alt={symbol}/> {symbol}</td>
            <td>{name}</td>
            <td>{price}</td>
            <td>{Math.round(change * 100) / 100} %</td>
        </tr>
    );
}

export default RecentChangesItem;