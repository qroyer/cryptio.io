import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from "react-router-dom";
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux'
import store from './store'

const style = document.createElement("link");
    style.setAttribute('href', '/dist/output.css');
    style.setAttribute('rel', 'stylesheet');

document.head.appendChild(style);

const container = document.getElementById('root');
const root = createRoot(container);
root.render(<React.StrictMode>
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>
</React.StrictMode>);

reportWebVitals();
