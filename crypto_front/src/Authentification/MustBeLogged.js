import React from 'react';
import {Navigate} from 'react-router-dom'
import {useSelector} from "react-redux";


function MustBeLogged() {

    const isLoogedIn = useSelector((state) => state.login.value)
    if (!isLoogedIn) {
        return <Navigate to='/login'/>
    }
}

export default MustBeLogged;
