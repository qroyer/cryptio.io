import {log_in, log_off} from './loginSlice'
import {useSelector, useDispatch} from 'react-redux'
import './login.css';
import {useState} from "react";
import React, {useEffect} from 'react';
import {getAllCryptoCurrencies, getUser, postUser} from "../Services/CurrencyAPI";
import {Navigate, NavLink} from "react-router-dom";

function Login() {
    const dispatch = useDispatch()
    const login = useSelector((state) => state.login.value)

    const [errorMessages, setErrorMessages] = useState({});
    const [user, setUser] = useState([]);

    useEffect(() => {
        getUser(user.username).then((response) => {
            setUser(
                response.data
            );
        });
    }, []);

    const errors = {
        uname: "invalid username",
        pass: "invalid password"
    };

    const handleSubmit = (event) => {
        event.preventDefault();

        let {uname, pass} = document.forms[0];

        postUser(uname.value, pass.value).then();

        if (pass.value === '') {
            setErrorMessages({name: "pass", message: errors.pass});
        }else if (uname.value === '') {
            setErrorMessages({name: "uname", message: errors.uname});
        } else {
            const user = {
                "pseudo": uname.value,
                "password": pass.value
            }
            dispatch(log_in(user));
        }
    };

    const renderErrorMessage = (name) =>
        name === errorMessages.name && (
            <div className="error">{errorMessages.message}</div>
        );

    const registerForm = (
        <div className="form">
            <form onSubmit={handleSubmit}>
                <div className="input-container">
                    <label>Username </label>
                    <input type="text" name="uname" required/>
                    {renderErrorMessage("uname")}
                </div>
                <div className="input-container">
                    <label>Password </label>
                    <input type="password" name="pass" required/>
                    {renderErrorMessage("pass")}
                </div>
                <div className="button-container">
                    <input type="submit" className="bg-orange"/>
                </div>
            </form>
        </div>
    );

    return (
        <div className="app max-w-container mx-auto">
            <div className="register-form">
                <div className="title">Register</div>
                {login ? <div>You are successfully registered !</div> : registerForm}
            </div>
        </div>
    );
}

export default Login;
