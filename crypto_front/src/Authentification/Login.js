import {log_in} from './loginSlice'
import {useSelector, useDispatch} from 'react-redux'
import './login.css';
import {useState} from "react";
import React, {useEffect} from 'react';
import {getUser} from "../Services/CurrencyAPI";
import {NavLink} from "react-router-dom";

function Login() {
    const login = useSelector((state) => state.login.value)
    const dispatch = useDispatch()

    const [errorMessages, setErrorMessages] = useState({});
    const [user, setUser] = useState([]);

    useEffect(() => {
        getUser(user.username).then((response) => {
            setUser(
                response.data
            );
        });
    }, []);

    const errors = {
        uname: "pseudo invalide",
        pass: "mot de passe invalide"
    };

    const handleSubmit = (event) => {
        event.preventDefault();

        let {uname, pass} = document.forms[0];

        getUser(uname.value).then((response) => {
            setUser(
                response.data
            );
        });

        if (user instanceof Object) {
            if (user.password !== pass.value) {
                setErrorMessages({name: "pass", message: errors.pass});
            } else {
                dispatch(log_in(user));
            }
            if (user.pseudo !== uname.value) {
                setErrorMessages({name: "uname", message: errors.uname});
            }
        } else {
            setErrorMessages({name: "uname", message: errors.uname});
        }
    };

    const renderErrorMessage = (name) =>
        name === errorMessages.name && (
            <div className="error">{errorMessages.message}</div>
        );

    const renderForm = (
        <div className="form">
            <form onSubmit={handleSubmit}>
                <div className="input-container">
                    <label>Pseudo </label>
                    <input type="text" name="uname" required/>
                    {renderErrorMessage("uname")}
                </div>
                <div className="input-container">
                    <label>Mot de passe </label>
                    <input type="password" name="pass" required/>
                    {renderErrorMessage("pass")}
                </div>
                <div className="button-container">
                    <input type="submit" className="bg-orange"/>
                    <NavLink id="register" to={"/register"} key={"/register"} className="ml-5 bg-orange">
                        Créer un compte
                    </NavLink>
                </div>
            </form>
        </div>
    );

    return (
        <div className="app">
            <div className="login-form">
                <div className="title">Connexion</div>
                {login ? <div>Vous êtes connecté !</div> : renderForm}
            </div>
        </div>
    );
}

export default Login;
