import {createSlice} from '@reduxjs/toolkit'

export const settingsCurrencySlice = createSlice({
    name: 'settingsCurrency',
    initialState: {
        value: 'eur',
    },
    reducers: {
        dollarCurrency: (state) => {
            state.value = 'usd'
        },
        eurosCurrency: (state) => {
            state.value = 'eur'
        }
    },
})

// Action creators are generated for each case reducer function
export const {dollarCurrency, eurosCurrency} = settingsCurrencySlice.actions

export default settingsCurrencySlice.reducer