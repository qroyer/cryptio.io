import Menu from "./../Menu"
import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux'
import {dollarCurrency, eurosCurrency} from './settingsCurrencySlice'
import MustBeLogged from "./MustBeLogged";

const Account = () => {

    const settingsCurrency = useSelector((state) => state.settingsCurrency.value)
    const dispatch = useDispatch()

    const handleSubmit = (event) => {
        event.preventDefault();

        let currency = document.getElementById('currency-select').value;

        if (currency === 'eur') {
            dispatch(eurosCurrency());
        }

        if (currency === 'usd') {
            dispatch(dollarCurrency());
        }

    };

    return (
        <div className="max-w-container mx-auto">
            <h1 className="text-3xl text-orange text-bold">Mon compte</h1>
            <MustBeLogged />
            <div className="form m-10 align-middle">
                <form onSubmit={handleSubmit} >
                    <label htmlFor="currency-select">Choississez une devise:</label>
                    <select name="currencies" id="currency-select">
                        <option value="eur">Euros</option>
                        <option value="usd">Dollar</option>
                    </select>
                    <div className="button-container">
                        <input type="submit" className={"bg-orange"} value="Sauvegarder"/>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default Account;

