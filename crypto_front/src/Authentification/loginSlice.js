import { createSlice } from '@reduxjs/toolkit'

export const loginSlice = createSlice({
    name: 'login',
    initialState: {
        value: false,
        pseudo: '',
    },
    reducers: {
        log_in: (state, test) => {
            state.value = true
            state.pseudo = test.payload.pseudo;
        },
        log_off: (state) => {
            state.value = false
            state.pseudo = "";
        },
    },
})

// Action creators are generated for each case reducer function
export const {log_in, log_off} = loginSlice.actions

export default loginSlice.reducer