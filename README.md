# Crypto.io 
Projet front en REACT sur la gestion d'un wallet de cryptomonnaie,
Quentin ROYER 

![hp](cryptoio.png)


## projet front

- React
  - Redux
- Docker
- Symfony
  - API Platform
- TailwindCSS
- Postgres

### Installation
```make up```
pour installer les services docker

```make ex``` Pour se connecter au service de l'application

```cd crypto_back && composer install ``` Installer le Back avec composer
```cd crypto_front && npm install ``` Installer le front avec NPM

Ensuite il faut installer la base :

```make initdb```

Si besoin de se connecter a la base : 
```make db    --->   psql -U database crypto ```


### starting project 

```make ex```
```cd crypto_front && npm start```


