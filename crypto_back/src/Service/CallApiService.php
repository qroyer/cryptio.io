<?php

declare(strict_types=1);

namespace App\Service;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiService
{
    public final const COIN_GECKO_API = 'https://api.coingecko.com/api/v3';
    public final const PING = '/ping';
    public final const COINS_LIST = '/coins/list';
    public final const COINS_MARKETS = '/coins/markets';
    public final const COINS = '/coins/';

    private HttpClientInterface $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    private function getApi(string $path): array
    {

        $response = $this->client->request(
            'GET',
            self::COIN_GECKO_API . $path
        );

        return $response->toArray();
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function ping(): array
    {
        return $this->getApi(self::PING);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function coinsList(): array
    {
        return $this->getApi(self::COINS_LIST);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function coinsMarkets(): array
    {
        return $this->getApi(self::COINS_MARKETS . '?vs_currency=btc&order=market_cap_desc&per_page=20&page=1&sparkline=true');
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function coinByName($name): array
    {
        return $this->getApi(self::COINS_MARKETS . '?vs_currency=eur&ids='.$name.'&order=market_cap_desc&per_page=100&page=1&sparkline=false' );
    }
}
