<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\GetCryptoByNameAction;
use App\Controller\PostCryptoAction;
use App\Repository\CryptoCurrencyRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: CryptoCurrencyRepository::class)]

/**
 * @UniqueEntity(fields="name", message="Name is already taken.")
 *
 * @ApiResource(
 *     collectionOperations={
 *         "get"={
 *             "method"="GET",
 *         },
 *         "post",
 *         "getBySymbol"={
 *             "method"="GET",
 *             "controller"=GetCryptoByNameAction::class,
 *             "path"="/crypto_currencies/symbol/{symbol}",
 *             "read"=false,
 *             "pagination_enabled"=false,
 *             "openapi_context"={
 *                 "summary"="Get by path parameters",
 *                 "parameters"={
 *                     {
 *                         "name" = "symbol",
 *                         "in" = "path",
 *                         "required" = true,
 *                         "type" = "string"
 *                     }
 *                 }
 *             }
 *         }
 *     }
 * )
 */
class CryptoCurrency
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 255)]
    private $symbol;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }
}
