<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\WalletRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WalletRepository::class)]
#[ApiResource]
class Wallet
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'float')]
    private $amout;

    #[ORM\ManyToOne(targetEntity: Users::class, inversedBy: 'wallets')]
    private $user_id;

    #[ORM\ManyToOne(targetEntity: CryptoCurrency::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $crypto_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmout(): ?float
    {
        return $this->amout;
    }

    public function setAmout(float $amout): self
    {
        $this->amout = $amout;

        return $this;
    }

    public function getUserId(): ?Users
    {
        return $this->user_id;
    }

    public function setUserId(?Users $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getCryptoId(): ?CryptoCurrency
    {
        return $this->crypto_id;
    }

    public function setCryptoId(?CryptoCurrency $crypto_id): self
    {
        $this->crypto_id = $crypto_id;

        return $this;
    }
}
