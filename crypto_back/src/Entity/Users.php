<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\GetByNameAction;
use App\Controller\PostUserAction;
use App\Repository\UsersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UsersRepository::class)]
#[ORM\Table(name: '`users`')]
/**
 * @ApiResource(
 *     collectionOperations={
 *         "post"={
 *             "method"="POST",
 *             "openapi_context": {
 *                 "requestBody": {
 *                     "content": {
 *                         "application/ld+json": {
 *                             "schema": {
 *                                 "type": "object",
 *                                 "properties": {
 *                                     "pseudo": {"type": "string", "example": "monpseudo"},
 *                                     "password": {"type": "string", "example": "123456"},
 *                                 },
 *                             },
 *                         },
 *                     },
 *                 },
 *             },
 *         },
 *         "get"={
 *             "method"="GET",
 *         },
 *         "get_by_path_params"={
 *             "method"="GET",
 *             "controller"=GetByNameAction::class,
 *             "path"="/user/pseudo/{pseudo}",
 *             "read"=false,
 *             "pagination_enabled"=false,
 *             "openapi_context"={
 *                 "summary"="Get by path parameters",
 *                 "parameters"={
 *                     {
 *                         "name" = "pseudo",
 *                         "in" = "path",
 *                         "required" = true,
 *                         "type" = "string"
 *                     }
 *                 }
 *             }
 *         },
 *     }
 * )
 */
class Users
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["get", "post"])]
    private $pseudo;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["get", "post"])]
    private $password;

    #[ORM\OneToMany(mappedBy: 'user_id', targetEntity: Wallet::class)]
    #[Groups(["get"])]
    private $wallets;

    public function __construct()
    {
        $this->wallets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection<int, Wallet>
     */
    public function getWallets(): Collection
    {
        return $this->wallets;
    }

    public function addWallet(Wallet $wallet): self
    {
        if (!$this->wallets->contains($wallet)) {
            $this->wallets[] = $wallet;
            $wallet->setUserId($this);
        }

        return $this;
    }

    public function removeWallet(Wallet $wallet): self
    {
        if ($this->wallets->removeElement($wallet)) {
            // set the owning side to null (unless already changed)
            if ($wallet->getUserId() === $this) {
                $wallet->setUserId(null);
            }
        }

        return $this;
    }
}
