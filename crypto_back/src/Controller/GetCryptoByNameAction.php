<?php

namespace App\Controller;

use App\Entity\Users;
use App\Repository\CryptoCurrencyRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\Response;

#[AsController]
class GetCryptoByNameAction extends AbstractController
{
    private CryptoCurrencyRepository $cryptoRepository;

    public function __construct(CryptoCurrencyRepository $cryptoRepository)
    {
        $this->cryptoRepository = $cryptoRepository;
    }

    /**
     * @throws NonUniqueResultException
     */
    public function __invoke($symbol): JsonResponse
    {
        $crypto = $this->cryptoRepository->findOneByName($symbol);

        if (!$crypto) {
            throw new \RuntimeException('Crypto not found');
        }

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $jsonContent = $serializer->serialize($crypto, 'json');

        $response = new JsonResponse();
        $response->setContent($jsonContent);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}