<?php

namespace App\Controller;

use App\Service\CallApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoinsListController extends AbstractController
{
    #[Route('/coins/list', name: 'app_coins_list')]
    public function index(CallApiService $callApiService): JsonResponse
    {
        return $this->json($callApiService->coinsList());
    }
}
