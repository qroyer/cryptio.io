<?php

namespace App\Controller;

use App\Entity\Users;
use App\Repository\UsersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class PostUserAction extends AbstractController
{
    private UsersRepository $usersRepository;

    public function __construct(UsersRepository $usersRepository) {
        $this->usersRepository = $usersRepository;

    }

    public function __invoke(string $pseudo, string $password)
    {
        $user = new Users();

        $user->setPseudo($pseudo)->setPassword($password);

        $this->usersRepository->add($user);
    }
}