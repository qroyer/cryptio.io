<?php

namespace App\Controller;

use App\Entity\CryptoCurrency;
use App\Entity\Users;
use App\Repository\CryptoCurrencyRepository;
use App\Repository\UsersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class PostCryptoAction extends AbstractController
{
    private CryptoCurrencyRepository $cryptosRepository;

    public function __construct(CryptoCurrencyRepository $cryptosRepository) {
        $this->cryptosRepository = $cryptosRepository;
    }

    public function __invoke(string $symbol)
    {
        $crypto = new CryptoCurrency();

        $crypto->setName($symbol);

        $this->cryptosRepository->add($crypto);
    }
}