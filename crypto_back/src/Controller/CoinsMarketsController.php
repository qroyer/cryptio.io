<?php

namespace App\Controller;

use App\Service\CallApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoinsMarketsController extends AbstractController
{
    #[Route('/coins/markets', name: 'app_coins_markets')]
    public function index(CallApiService $callApiService): JsonResponse
    {
        return $this->json($callApiService->coinsMarkets());
    }
}
