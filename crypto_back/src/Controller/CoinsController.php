<?php

namespace App\Controller;

use App\Service\CallApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoinsController extends AbstractController
{
    #[Route('/coins/markets/{name}', name: 'app_coins')]
    public function index(CallApiService $callApiService, string $name): JsonResponse
    {
        return $this->json($callApiService->coinByName($name));
    }
}
